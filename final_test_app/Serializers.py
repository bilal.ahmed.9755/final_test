from rest_framework import serializers
from .models import Account, Account_log, Transaction_Log


class Account_Balance_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['balance']


class Account_Name_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['user_name']


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'

    def validate(self, data):
        if Account.objects.filter(account_no=data['account_no']).exists():
            raise serializers.ValidationError("Account Already Exists!")
        elif data['balance'] < 5000:
            raise serializers.ValidationError("plz deposit atleast 5000/_")
        else:
            return data


class TransactionLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction_Log
        fields = '__all__'


class Account_logSerializer(serializers.ModelSerializer):
    account = Account_Name_Serializer(many=False, read_only=True)

    class Meta:
        model = Account_log
        fields = ['account', 'type', 'amount', 'date', 'time']
