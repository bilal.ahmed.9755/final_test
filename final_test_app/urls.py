from django.urls import path
from . import views

urlpatterns = [

    path('account_details/<str:account_no>/', views.AccountDetails.as_view(), name='AccountDetails'),
    path('deposit_withdraw/<str:transaction_type>/<str:account_no>/<int:amount>/', views.Deposit_Withdraw.as_view(),
         name='Deposit_Withdraw'),
    path('transfer/<str:sender>/<str:receiver>/<int:amount>/', views.Transfer.as_view(), name='Transfer'),
    path('accounts/', views.Accounts.as_view(), name='Accounts'),
    path('account_log/<str:account_no>/', views.AccountLog.as_view(), name="AccountLog"),
    path('check_balance/<str:account_no>/', views.Check_Balance.as_view(), name='check_balance'),
    path('sender_log/<str:account_no>/', views.Sender_Log.as_view(), name='sender_log'),
    path('receiver_log/<str:account_no>/', views.Receiver_Log.as_view(), name='receiver_log')

]
