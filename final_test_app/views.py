from rest_framework.views import APIView
from .models import Account, Account_log, Transaction_Log
from .Serializers import AccountSerializer, TransactionLogSerializer, Account_logSerializer, Account_Balance_Serializer
from rest_framework.response import Response
from datetime import datetime


# Create your views here.


class AccountDetails(APIView):
    # get and delete account on account_no
    @staticmethod
    def get(request, account_no):
        account_detail = Account.objects.get(account_no=account_no)
        serial_obj = AccountSerializer(account_detail)
        return Response(serial_obj.data)

    @staticmethod
    def delete(request, account_no):
        account = Account.objects.get(account_no=account_no)
        account.delete()


class Deposit_Withdraw(APIView):
    # account debit and credit
    @staticmethod
    def get(request, transaction_type, account_no, amount):
        date = datetime.now().date()
        time = datetime.now().time()
        record = Account.objects.get(account_no=account_no)
        if transaction_type == 'debit':
            old_balance = record.balance
            new_balance = old_balance + amount
            record.balance = new_balance
            record.save()
            entry = Account_log(account=record, amount=amount, type=transaction_type, date=date, time=time)
            entry.save()
            serial_obj = AccountSerializer(record)
            return Response(serial_obj.data)
        else:
            if transaction_type == 'credit':
                if record.balance >= amount:
                    old_balance = record.balance
                    new_balance = old_balance - amount
                    record.balance = new_balance
                    record.save()
                    entry = Account_log(account=record, amount=amount, type=transaction_type, date=date, time=time)
                    entry.save()
                    serial_obj = AccountSerializer(record)
                    return Response(serial_obj.data)
                else:
                    return Response(status=400)


class Transfer(APIView):

    @staticmethod
    def get(request, sender, receiver, amount):
        # deducting amount from sender and adding into receiver
        # entry of transaction in both logs
        date = datetime.now().date()
        time = datetime.now().time()
        sender = Account.objects.get(account_no=sender)
        receiver = Account.objects.get(account_no=receiver)
        if sender.balance >= amount:
            sender.balance = sender.balance - amount
            receiver.balance = receiver.balance + amount
            sender.save()
            receiver.save()
            sender_log = Account_log(account=sender, amount=amount, type="credit", date=date, time=time)
            receiver_log = Account_log(account=receiver, amount=amount, type="debit", date=date, time=time)
            sender_log.save()
            receiver_log.save()
            transaction_log = Transaction_Log(sender=sender, receiver=receiver, amount=amount, date=date, time=time)
            transaction_log.save()
            serial_obj = TransactionLogSerializer(transaction_log)
            return Response(serial_obj.data)

        else:
            return Response(status=400)


class Accounts(APIView):
    @staticmethod
    def get(request):
        all_accounts = Account.objects.all()
        serial_obj = AccountSerializer(all_accounts, many=True)
        return Response(serial_obj.data)

    @staticmethod
    def post(request):
        serial_obj = AccountSerializer(data=request.data)
        if serial_obj.is_valid():
            serial_obj.save()
            return Response(serial_obj.data)
        else:
            return Response(serial_obj.errors)


class AccountLog(APIView):
    @staticmethod
    def get(request, account_no):
        account_log = Account_log.objects.filter(account__account_no=account_no)
        serial_obj = Account_logSerializer(account_log, many=True)
        return Response(serial_obj.data)


class Check_Balance(APIView):
    @staticmethod
    def get(request, account_no):
        balance = Account.objects.get(account_no=account_no)
        serial_obj = Account_Balance_Serializer(balance)
        return Response(serial_obj.data)


class Sender_Log(APIView):
    @staticmethod
    def get(request, account_no):
        sender_log = Transaction_Log.objects.filter(sender__account_no=account_no)
        serial_obj = TransactionLogSerializer(sender_log, many=True)
        return Response(serial_obj.data)


class Receiver_Log(APIView):
    @staticmethod
    def get(request, account_no):
        receiver_log = Transaction_Log.objects.filter(receiver__account_no=account_no)
        serial_obj = TransactionLogSerializer(receiver_log, many=True)
        return Response(serial_obj.data)
