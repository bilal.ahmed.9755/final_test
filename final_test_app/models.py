from django.db import models

# Create your models here.


class Account(models.Model):

    user_name = models.CharField(max_length=30)
    account_no = models.CharField(max_length=30)
    phone_no = models.CharField(max_length=11)
    balance = models.IntegerField()


class Log(models.Model):
    date = models.DateField()
    time = models.TimeField()

    class Meta:
        abstract = True


class Account_log(Log):

    TYPE = {
        ('debit', 'debit'),
        ('credit', 'credit')
    }
    amount = models.IntegerField()
    type = models.CharField(max_length=20, choices=TYPE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)


class Transaction_Log(Log):
    sender = models.ForeignKey(Account, related_name='sender', on_delete=models.CASCADE)
    receiver = models.ForeignKey(Account, related_name='receiver', on_delete=models.CASCADE)
    amount = models.IntegerField()
