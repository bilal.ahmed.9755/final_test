from django.apps import AppConfig


class FinalTestAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'final_test_app'
