from django.contrib import admin
from .models import Account, Account_log, Transaction_Log

# Register your models here.
admin.site.register(Account)
admin.site.register(Account_log)
admin.site.register(Transaction_Log)
